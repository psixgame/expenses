package com.psixgame.apps.expenses.expenses.data;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.room.Room;

@RunWith(AndroidJUnit4.class)
public class ExpenseDaoTest {

    private ExpensesDatabase db;

    @Before
    public void setUp() {
        db = Room.inMemoryDatabaseBuilder(
                InstrumentationRegistry.getContext(),
                ExpensesDatabase.class)
                .allowMainThreadQueries()
                .build();
    }

    @After
    public void tearDown() {
        db.close();
    }

    @Rule
    public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();

    private static final ExpenseEntity entity1;
    private static final ExpenseEntity entity2;
    private static final ExpenseEntity entity3;

    static {
        entity1 = new ExpenseEntity();
        entity1.setCreatedAt(1);
        entity1.setDate("date1");
        entity1.setSpent(1);
        entity1.setType("type1");
        entity1.setSubtype("subtype1");
        entity1.setComment("comment1");

        entity2 = new ExpenseEntity();
        entity2.setCreatedAt(2);
        entity2.setDate("date2");
        entity2.setSpent(-2);
        entity2.setType("type2");
        entity2.setSubtype("subtype2");
        entity2.setComment("comment2");

        entity3 = new ExpenseEntity();
        entity3.setCreatedAt(3);
        entity3.setDate("date3");
        entity3.setSpent(3);
        entity3.setType("type3");
        entity3.setSubtype("subtype3");
        entity3.setComment("comment3");
    }

    @Test
    public void name() {
        db.expenseDao().insert(entity1);
        db.expenseDao().insert(entity2);
        db.expenseDao().insert(entity3);

        Assert.assertEquals(3, db.expenseDao().getAll().size());

        db.expenseDao().delete(entity2);

        Assert.assertEquals(2, db.expenseDao().getAll().size());

        for (final ExpenseEntity e : db.expenseDao().getAll()) {
            Assert.assertFalse(e.getCreatedAt() == entity2.getCreatedAt());
        }

        db.expenseDao().delete(entity1);
        db.expenseDao().delete(entity3);

        Assert.assertEquals(0, db.expenseDao().getAll().size());

        db.expenseDao().insert(entity1);
        db.expenseDao().insert(entity2);
        db.expenseDao().insert(entity3);

        Assert.assertEquals(entity1, db.expenseDao().getFirst());

        db.expenseDao().delete(entity1);

        Assert.assertEquals(entity2, db.expenseDao().getFirst());
    }

    @Test
    public void name2() {
        db.expenseDao().insert(entity1);
        db.expenseDao().insert(entity2);
        db.expenseDao().getAllAsync().test().assertValue(v -> v.size() == 2);
    }
}
