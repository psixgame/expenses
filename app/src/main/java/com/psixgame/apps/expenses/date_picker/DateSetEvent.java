package com.psixgame.apps.expenses.date_picker;

public final class DateSetEvent {

    private final int year, month, day;

    DateSetEvent(final int year, final int month, final int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public final int getYear() {
        return year;
    }

    public final int getMonth() {
        return month;
    }

    public final int getDay() {
        return day;
    }

    @Override
    public final String toString() {
        return "DateSetEvent{" +
                "year=" + year +
                ", month=" + month +
                ", day=" + day +
                '}';
    }
}
