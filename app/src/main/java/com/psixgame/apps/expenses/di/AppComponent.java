package com.psixgame.apps.expenses.di;

import com.psixgame.apps.expenses.expenses.data.ExpensesDataModule;
import com.psixgame.apps.expenses.presentation.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        AppModule.class,
        ExpensesDataModule.class
})
public interface AppComponent {

    void inject(MainActivity activity);

}
