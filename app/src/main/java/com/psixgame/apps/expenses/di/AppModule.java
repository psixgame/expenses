package com.psixgame.apps.expenses.di;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public final class AppModule {

    private final Application application;

    public AppModule(final Application application) {
        this.application = application;
    }

    @Singleton
    @Provides
    final Context provideContext() {
        return application;
    }

}
