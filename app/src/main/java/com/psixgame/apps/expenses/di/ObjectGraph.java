package com.psixgame.apps.expenses.di;

import android.app.Application;

public class ObjectGraph {

    private static ObjectGraph graph;

    public static ObjectGraph getInstance(final Application application) {
        if (graph == null) {
            graph = new ObjectGraph(application);
        }
        return graph;
    }

    private final AppComponent appComponent;

    private ObjectGraph(final Application application) {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(application))
                .build();
    }

    public final AppComponent getAppComponent() {
        return appComponent;
    }
}
