package com.psixgame.apps.expenses.expenses;

import java.util.Objects;

public final class ExpenseData {

    private final long createdAt;
    private final String date;
    private final float spent;
    private final String type;
    private final String subtype;
    private final String comment;

    private ExpenseData(final Builder builder) {
        createdAt = builder.createdAt;
        date = builder.date;
        spent = builder.spent;
        type = builder.type;
        subtype = builder.subtype;
        comment = builder.comment;
    }

    public final long getCreatedAt() {
        return createdAt;
    }

    public final String getDate() {
        return date;
    }

    public final float getSpent() {
        return spent;
    }

    public final String getType() {
        return type;
    }

    public final String getSubtype() {
        return subtype;
    }

    public final String getComment() {
        return comment;
    }

    public static class Builder {

        private long createdAt;
        private String date;
        private float spent;
        private String type;
        private String subtype;
        private String comment;

        public final Builder setCreatedAt(final long createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public final Builder setDate(final String date) {
            this.date = date;
            return this;
        }

        public final Builder setSpent(final float spent) {
            this.spent = spent;
            return this;
        }

        public final Builder setType(final String type) {
            this.type = type;
            return this;
        }

        public final Builder setSubtype(final String subtype) {
            this.subtype = subtype;
            return this;
        }

        public final Builder setComment(final String comment) {
            this.comment = comment;
            return this;
        }

        public final ExpenseData build() {
            if (createdAt <= 0) throw new AssertionError("createdAt should be set");
            Objects.requireNonNull(date, "date should be set");
            if (spent == 0) throw new AssertionError("spent should be set");
            Objects.requireNonNull(type, "type should be set");
            return new ExpenseData(this);
        }
    }

}
