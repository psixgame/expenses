package com.psixgame.apps.expenses.expenses;

import com.psixgame.apps.expenses.expenses.data.ExpensesPersister;
import com.psixgame.apps.expenses.google.GoogleSheets;
import com.psixgame.apps.expenses.utils.network.ConnectionStatusMessage;
import com.psixgame.apps.expenses.utils.network.NetworkUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public final class ExpensesRepository {

    private final GoogleSheets googleSheets;
    private final NetworkUtils networkUtils;
    private final ExpensesPersister persister;

    @Inject
    public ExpensesRepository(final GoogleSheets googleSheets,
                              final NetworkUtils networkUtils,
                              final ExpensesPersister persister) {
        this.googleSheets = googleSheets;
        this.networkUtils = networkUtils;
        this.persister = persister;
        EventBus.getDefault().register(this);
    }

    public final void add(final ExpenseData data) {
        persister.save(data, this::added);
    }

    private void added() {
        if (uploading) return;

        if (networkUtils.isDeviceOnline()) {
            process();
        }
    }

    private boolean uploading = false;

    private void process() {
        uploading = true;
        notifySyncing(uploading);

        persister.count(count -> {
            notifyCount(count);
            if (count == 0) {
                uploading = false;
                notifySyncing(uploading);
                return;
            }

            persister.getFirst(expense ->
                    googleSheets.append(expense, () ->
                                    persister.delete(expense,
                                            this::process),
                            error -> {
                                uploading = false;
                                notifySyncing(uploading);
                            }));
        });
    }

    @Subscribe
    public final void onMessage(final ConnectionStatusMessage message) {
        if (!uploading && message.isEnabled()) {
            process();
        }
    }

    public final boolean isSyncing() {
        return uploading;
    }

    private final List<Consumer<Integer>> pendingCountListeners = new ArrayList<>();

    public final void addPendingCountListener(final Consumer<Integer> listener) {
        pendingCountListeners.add(listener);
    }

    public final void removePendingCountListener(final Consumer<Integer> listener) {
        pendingCountListeners.remove(listener);
    }

    private void notifyCount(final int count) {
        for (final Consumer<Integer> listener : pendingCountListeners) {
            listener.accept(count);
        }
    }

    private final List<Consumer<Boolean>> syncingListeners = new ArrayList<>();

    public final void addSyncingListener(final Consumer<Boolean> listener) {
        syncingListeners.add(listener);
    }

    public final void removeSyncingListener(final Consumer<Boolean> listener) {
        syncingListeners.remove(listener);
    }

    private void notifySyncing(final boolean syncing) {
        for (final Consumer<Boolean> listener : syncingListeners) {
            listener.accept(syncing);
        }
    }

}
