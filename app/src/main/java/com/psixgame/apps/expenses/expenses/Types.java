package com.psixgame.apps.expenses.expenses;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Types {

    private static final List<String> types = Arrays.asList(
            "Їжа",
            "Побут",
            "Одяг",
            "Техніка і речі",
            "Розваги",
            "Квартира і звязок",
            "Різне",
            "Фінанси",
            "Транспорт"
    );

    private static final Map<String, List<String>> subtypes;

    static {
        subtypes = new HashMap<>();

        subtypes.put("Їжа", Arrays.asList(
                "Солодке",
                "Вода",
                "Молочні продукти",
                "Хлібобулочні вироби",
                "Мясо і птиця",
                "Готова",
                "Овочі і фрукти",
                "Сипучі продукти",
                "Столова",
                "Для приготування: спеції, соуси і т.д."
        ));

        subtypes.put("Побут", Arrays.asList(
                "Засоби гігієни",
                "Ванна",
                "Медицина",
                "Стрижка"
        ));

        subtypes.put("Одяг", Collections.emptyList());

        subtypes.put("Техніка і речі", Collections.emptyList());

        subtypes.put("Розваги", Arrays.asList(
                "Алкоголь",
                "Кафе",
                "Кава",
                "Відпочинок"
        ));

        subtypes.put("Квартира і звязок", Arrays.asList(
                "Поповнення тел",
                "Поповнення тел інше",
                "Інтернет",
                "Інтернет інше",
                "Квартплата",
                "Ремонт"
        ));

        subtypes.put("Різне", Arrays.asList(
                "Проїзд",
                "Зал",
                "Подарунки"
        ));

        subtypes.put("Фінанси", Arrays.asList(
                "Борги",
                "Обмін",
                "Комісія",
                "З/п",
                "Податки"
        ));

        subtypes.put("Транспорт", Arrays.asList(
                "Пальне",
                "Мийка",
                "ТО",
                "Ремонт"
        ));
    }

    public static List<String> getTypes() {
        return new ArrayList<>(types);
    }

    public static List<String> getSubtypes(final String type) {
        if (TextUtils.isEmpty(type)) return new ArrayList<>();

        if (!subtypes.containsKey(type)) throw new AssertionError();

        return new ArrayList<>(subtypes.get(type));
    }

}
