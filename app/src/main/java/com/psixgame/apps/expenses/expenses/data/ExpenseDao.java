package com.psixgame.apps.expenses.expenses.data;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Single;

@Dao
interface ExpenseDao {

    @Query("SELECT * FROM expenses ORDER BY createdAt ASC")
    List<ExpenseEntity> getAll();

    @Query("SELECT * FROM expenses ORDER BY createdAt ASC")
    Single<List<ExpenseEntity>> getAllAsync();

    @Query("SELECT * FROM expenses ORDER BY createdAt ASC LIMIT 1")
    ExpenseEntity getFirst();

    @Insert
    void insert(ExpenseEntity... entities);

    @Delete
    void delete(ExpenseEntity entity);

    @Query("SELECT COUNT() FROM expenses")
    int count();

}
