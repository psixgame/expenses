package com.psixgame.apps.expenses.expenses.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity(tableName = "expenses")
class ExpenseEntity {

    @PrimaryKey
    private long createdAt;
    private String date;
    private float spent;
    private String type;
    private String subtype;
    private String comment;

    public final long getCreatedAt() {
        return createdAt;
    }

    public final void setCreatedAt(final long createdAt) {
        this.createdAt = createdAt;
    }

    public final String getDate() {
        return date;
    }

    public final void setDate(final String date) {
        this.date = date;
    }

    public final float getSpent() {
        return spent;
    }

    public final void setSpent(final float spent) {
        this.spent = spent;
    }

    public final String getType() {
        return type;
    }

    public final void setType(final String type) {
        this.type = type;
    }

    public final String getSubtype() {
        return subtype;
    }

    public final void setSubtype(final String subtype) {
        this.subtype = subtype;
    }

    public final String getComment() {
        return comment;
    }

    public final void setComment(final String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExpenseEntity that = (ExpenseEntity) o;
        return createdAt == that.createdAt &&
                Float.compare(that.spent, spent) == 0 &&
                Objects.equals(date, that.date) &&
                Objects.equals(type, that.type) &&
                Objects.equals(subtype, that.subtype) &&
                Objects.equals(comment, that.comment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(createdAt, date, spent, type, subtype, comment);
    }
}
