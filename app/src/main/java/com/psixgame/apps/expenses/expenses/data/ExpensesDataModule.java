package com.psixgame.apps.expenses.expenses.data;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public final class ExpensesDataModule {

    @Singleton
    @Provides
    final ExpensesPersister provideExpensesPersister(final Context context,
                                                     final Mapper mapper) {
        return new RoomExpensesPersister(context, mapper);
    }

}
