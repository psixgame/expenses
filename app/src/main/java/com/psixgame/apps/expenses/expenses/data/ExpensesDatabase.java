package com.psixgame.apps.expenses.expenses.data;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {ExpenseEntity.class}, version = 1)
abstract class ExpensesDatabase extends RoomDatabase {
    public abstract ExpenseDao expenseDao();
}
