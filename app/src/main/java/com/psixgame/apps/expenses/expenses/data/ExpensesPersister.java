package com.psixgame.apps.expenses.expenses.data;

import com.psixgame.apps.expenses.expenses.ExpenseData;

import java.util.List;
import java.util.function.Consumer;

public interface ExpensesPersister {

    void save(ExpenseData data, Runnable onComplete);
    void getAll(Consumer<List<ExpenseData>> onComplete);
    void getFirst(Consumer<ExpenseData> onComplete);
    void delete(ExpenseData data, Runnable onComplete);
    void delete(List<ExpenseData> datas, Runnable onComplete);
    void count(Consumer<Integer> onComplete);

}
