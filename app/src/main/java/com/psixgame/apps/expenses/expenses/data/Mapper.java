package com.psixgame.apps.expenses.expenses.data;

import com.psixgame.apps.expenses.expenses.ExpenseData;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
final class Mapper {

    @Inject
    Mapper() {
    }

    final ExpenseEntity map(final ExpenseData data) {
        final ExpenseEntity e = new ExpenseEntity();
        e.setCreatedAt(data.getCreatedAt());
        e.setDate(data.getDate());
        e.setSpent(data.getSpent());
        e.setType(data.getType());
        e.setSubtype(data.getSubtype());
        e.setComment(data.getComment());
        return e;
    }

    final List<ExpenseEntity> mapDatas(final List<ExpenseData> data) {
        final List<ExpenseEntity> list = new ArrayList<>(data.size());
        for (final ExpenseData d : data) {
            list.add(map(d));
        }
        return list;
    }

    final ExpenseData map(final ExpenseEntity entity) {
        return new ExpenseData.Builder()
                .setCreatedAt(entity.getCreatedAt())
                .setDate(entity.getDate())
                .setSpent(entity.getSpent())
                .setType(entity.getType())
                .setSubtype(entity.getSubtype())
                .setComment(entity.getComment())
                .build();
    }

    final List<ExpenseData> mapEntities(final List<ExpenseEntity> in) {
        final List<ExpenseData> res = new ArrayList<>(in.size());
        for (final ExpenseEntity e : in) {
            res.add(map(e));
        }
        return res;
    }

}
