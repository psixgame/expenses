package com.psixgame.apps.expenses.expenses.data;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.room.Room;

import com.psixgame.apps.expenses.expenses.ExpenseData;

import java.util.List;
import java.util.function.Consumer;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@SuppressLint({"CheckResult", "NewApi"})
@Singleton
final class RoomExpensesPersister implements ExpensesPersister {

    private final ExpensesDatabase database;
    private final ExpenseDao dao;
    private final Mapper mapper;

    @Inject
    RoomExpensesPersister(final Context context,
                          final Mapper mapper) {
        this.mapper = mapper;
        database = Room.databaseBuilder(context, ExpensesDatabase.class, "expenses-database").build();
        dao = database.expenseDao();
    }

    @Override
    public final void save(final ExpenseData data, final Runnable onComplete) {
        Completable.fromAction(() -> dao.insert(mapper.map(data)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onComplete::run);
    }

    @Override
    public final void getAll(final Consumer<List<ExpenseData>> onComplete) {
        Single.fromCallable(() -> mapper.mapEntities(dao.getAll()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onComplete::accept);
    }

    @Override
    public final void getFirst(final Consumer<ExpenseData> onComplete) {
        Single.fromCallable(() -> mapper.map(dao.getFirst()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onComplete::accept);
    }

    @Override
    public final void delete(final ExpenseData data, final Runnable onComplete) {
        Completable.fromAction(() -> dao.delete(mapper.map(data)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onComplete::run);
    }

    @Override
    public final void delete(final List<ExpenseData> datas, final Runnable onComplete) {
        Completable.fromAction(() -> {
            for (final ExpenseEntity e : mapper.mapDatas(datas)) {
                dao.delete(e);
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onComplete::run);
    }

    @Override
    public final void count(final Consumer<Integer> onComplete) {
        Single.fromCallable(dao::count)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onComplete::accept);
    }
}
