package com.psixgame.apps.expenses.google;

import static android.app.Activity.RESULT_OK;

import android.Manifest;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.psixgame.apps.expenses.utils.KeyValueStorage;

import java.util.Arrays;
import java.util.function.Consumer;

import javax.inject.Inject;
import javax.inject.Singleton;

@SuppressLint("NewApi")
@Singleton
public final class GoogleAccount {

    private final KeyValueStorage keyValueStorage;

    private static final String[] SCOPES = {SheetsScopes.SPREADSHEETS};
    private final GoogleAccountCredential credential;

    @Inject
    GoogleAccount(final Context context,
                  final KeyValueStorage keyValueStorage) {
        this.keyValueStorage = keyValueStorage;

        credential = GoogleAccountCredential.usingOAuth2(
                context, Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff());
    }

    public final GoogleAccountCredential getCredential() {
        return credential;
    }

    private Runnable success;
    private Consumer<String> error;

    public final void check(final Activity activity, final Runnable success, final Consumer<String> error) {
        this.success = success;
        this.error = error;

        if (credential.getSelectedAccountName() == null) {
            checkPermission(activity);
        } else {
            this.success.run();
        }
    }

    private static final String KEY_ACCOUNT_NAME = "accountName";
    private static final int REQUEST_ACCOUNT_PICKER = 1000;

    private void checkPermission(final Activity activity) {
        Dexter.withActivity(activity)
                .withPermission(Manifest.permission.GET_ACCOUNTS)
                .withListener(new PermissionListener() {

                    @Override
                    public void onPermissionGranted(final PermissionGrantedResponse response) {
                        if (keyValueStorage.has(KEY_ACCOUNT_NAME)) {
                            credential.setSelectedAccountName(keyValueStorage.read(KEY_ACCOUNT_NAME));
                            success.run();
                        } else {
                            chooseAccount(activity);
                        }
                    }

                    @Override
                    public void onPermissionDenied(final PermissionDeniedResponse response) {
                        error.accept("Permission denied: " + response.getPermissionName());
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(final PermissionRequest permission, final PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .check();
    }

    private void chooseAccount(final Activity activity) {
        activity.startActivityForResult(credential.newChooseAccountIntent(), REQUEST_ACCOUNT_PICKER);
    }

    private void accountPickerResponse(final int resultCode, final Intent data) {
        if (resultCode == RESULT_OK && data != null && data.getExtras() != null) {
            if (data.hasExtra(AccountManager.KEY_ACCOUNT_NAME)) {
                final String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                keyValueStorage.save(KEY_ACCOUNT_NAME, accountName);
                credential.setSelectedAccountName(accountName);
                success.run();
            }
        } else {
            error.accept("You must select google account");
        }
    }

    public final void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        switch (requestCode) {
            case REQUEST_ACCOUNT_PICKER:
                accountPickerResponse(resultCode, data);
                break;
        }
    }
}
