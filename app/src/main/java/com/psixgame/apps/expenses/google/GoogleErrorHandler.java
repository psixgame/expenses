package com.psixgame.apps.expenses.google;

import static android.app.Activity.RESULT_OK;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.psixgame.apps.expenses.utils.CurrentActivityProvider;

import java.util.function.Consumer;

import javax.inject.Inject;
import javax.inject.Singleton;

@SuppressLint("NewApi")
@Singleton
public final class GoogleErrorHandler {

    private static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;

    private final CurrentActivityProvider activityProvider;

    @Inject
    GoogleErrorHandler(final CurrentActivityProvider activityProvider) {
        this.activityProvider = activityProvider;
    }

    public final void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        switch (requestCode) {
            case REQUEST_GOOGLE_PLAY_SERVICES:
                handleGooglePlayServicesResponse(resultCode == RESULT_OK);
                break;

            case REQUEST_AUTHORIZATION:
                handleAuthorizationResponse(resultCode == RESULT_OK);
                break;
        }
    }

    private Runnable servicesSuccess;
    private Consumer<String> servicesError;

    public final void showGooglePlayServicesAvailabilityErrorDialog(final int connectionStatusCode, final Runnable success, final Consumer<String> error) {
        servicesSuccess = success;
        servicesError = error;
        final GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        final Dialog dialog = apiAvailability.getErrorDialog(activityProvider.getActivity(), connectionStatusCode, REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    private void handleGooglePlayServicesResponse(final boolean success) {
        if (success) {
            servicesSuccess.run();
        } else {
            servicesError.accept("This app requires Google Play Services. Please install " +
                    "Google Play Services on your device and relaunch this app.");
        }
    }

    private static final int REQUEST_AUTHORIZATION = 1001;

    private Runnable sheetsSuccess;
    private Consumer<String> sheetsError;

    @RequiresApi(api = Build.VERSION_CODES.N)

    public final void handleSheetsError(final Throwable throwable, final Runnable success, final Consumer<String> error) {
        sheetsSuccess = success;
        sheetsError = error;
        if (throwable instanceof GooglePlayServicesAvailabilityIOException) {
            showGooglePlayServicesAvailabilityErrorDialog(((GooglePlayServicesAvailabilityIOException) throwable).getConnectionStatusCode(), success, error);
        } else if (throwable instanceof UserRecoverableAuthIOException) {
            activityProvider.getActivity().startActivityForResult(((UserRecoverableAuthIOException) throwable).getIntent(), REQUEST_AUTHORIZATION);
        } else {
            sheetsError.accept("The following error occurred:\n" + throwable.getCause());
        }
    }

    private void handleAuthorizationResponse(final boolean success) {
        if (success) {
            sheetsSuccess.run();
        } else {
            sheetsError.accept("Authorization not granted");
        }
    }

}
