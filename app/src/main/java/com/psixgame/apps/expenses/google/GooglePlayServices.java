package com.psixgame.apps.expenses.google;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.util.function.Consumer;

import javax.inject.Inject;
import javax.inject.Singleton;

@SuppressLint("NewApi")
@Singleton
public final class GooglePlayServices {

    private final GoogleErrorHandler googleErrorHandler;

    @Inject
    public GooglePlayServices(final GoogleErrorHandler googleErrorHandler) {
        this.googleErrorHandler = googleErrorHandler;
    }

    private Runnable success;
    private Consumer<String> error;

    public final void check(final Activity activity, final Runnable success, final Consumer<String> error) {
        if (isGooglePlayServicesAvailable(activity.getApplicationContext())) {
            success.run();
        } else {
            acquireGooglePlayServices(activity.getApplicationContext(), success, error);
        }
    }

    private boolean isGooglePlayServicesAvailable(final Context context) {
        final GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        final int connectionStatusCode = apiAvailability.isGooglePlayServicesAvailable(context);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }

    private void acquireGooglePlayServices(final Context context, final Runnable success, final Consumer<String> error) {
        final GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        final int connectionStatusCode = apiAvailability.isGooglePlayServicesAvailable(context);
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            googleErrorHandler.showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode, success, error);
        } else {
            error.accept("Google Play Services error: " + connectionStatusCode);
        }
    }

    public final void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        googleErrorHandler.onActivityResult(requestCode, resultCode, data);
    }
}
