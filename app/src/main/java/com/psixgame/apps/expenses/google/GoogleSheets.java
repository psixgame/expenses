package com.psixgame.apps.expenses.google;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.AppendValuesResponse;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.psixgame.apps.expenses.expenses.ExpenseData;
import com.psixgame.apps.expenses.utils.SheetUpdateService;

import java.io.IOException;
import java.util.Arrays;
import java.util.function.Consumer;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@SuppressLint({"CheckResult", "NewApi"})
@Singleton
public final class GoogleSheets {

    private final Context context;
    private final GoogleErrorHandler googleErrorHandler;
    private final GoogleAccount googleAccount;

    @Inject
    GoogleSheets(final Context context,
                 final GoogleErrorHandler googleErrorHandler,
                 final GoogleAccount googleAccount) {
        this.context = context;
        this.googleErrorHandler = googleErrorHandler;
        this.googleAccount = googleAccount;
    }

    public final void append(final ExpenseData data, final Runnable success, final Consumer<String> error) {
        Single.fromCallable(() -> append(data))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(count -> onSuccess(success),
                        throwable -> onError(data, success, throwable, error));
    }

    private final String SHEET_ID = "15aYKh2UmizUaA6Acu0J-4tt9f33tYRmYkyzTKDMZK_E";
    //test sheet
//    private static final String SHEET_ID = "1hWaaLvJdmQJyq2ymC_JPIv4NznYjyOvkqKBDHA8YeI4";
    private static final String TABLE_NAME = "Операції";

    private int append(final ExpenseData expenseData) throws IOException {
        context.startService(new Intent(context, SheetUpdateService.class));

        final HttpTransport transport = AndroidHttp.newCompatibleTransport();
        final JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        final Sheets sheets = new Sheets.Builder(
                transport, jsonFactory, googleAccount.getCredential())
                .build();

        final ValueRange range = new ValueRange()
                .setValues(Arrays.asList(
                        Arrays.asList(
                                expenseData.getDate(),
                                expenseData.getSpent(),
                                expenseData.getType(),
                                expenseData.getSubtype(),
                                expenseData.getComment()
                        )
                ));

        final AppendValuesResponse response = sheets.spreadsheets().values()
                .append(SHEET_ID, TABLE_NAME, range)
                .setValueInputOption("USER_ENTERED")
                .execute();

        return response.getUpdates().getUpdatedCells();
    }

    private void onSuccess(final Runnable success) {
        context.stopService(new Intent(context, SheetUpdateService.class));
        success.run();
    }

    private void onError(final ExpenseData data, final Runnable success, final Throwable throwable, final Consumer<String> error) {
        context.stopService(new Intent(context, SheetUpdateService.class));
        googleErrorHandler.handleSheetsError(throwable, () -> append(data, success, error), error);
    }

}
