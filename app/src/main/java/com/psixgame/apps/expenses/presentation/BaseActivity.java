package com.psixgame.apps.expenses.presentation;

import androidx.appcompat.app.AppCompatActivity;

import com.psixgame.apps.expenses.utils.CurrentActivityProvider;

import javax.inject.Inject;

public abstract class BaseActivity extends AppCompatActivity {

    @Inject
    CurrentActivityProvider currentActivityProvider;

    @Override
    protected void onStart() {
        super.onStart();
        currentActivityProvider.setActivity(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        currentActivityProvider.removeActivity(this);
    }

}
