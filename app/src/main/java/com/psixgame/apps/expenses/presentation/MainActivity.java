package com.psixgame.apps.expenses.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.psixgame.apps.expenses.databinding.ActivityMainBinding;
import com.psixgame.apps.expenses.date_picker.DatePickerDialogFragment;
import com.psixgame.apps.expenses.date_picker.DateSetEvent;
import com.psixgame.apps.expenses.di.ObjectGraph;
import com.psixgame.apps.expenses.expenses.ExpenseData;
import com.psixgame.apps.expenses.expenses.ExpensesRepository;
import com.psixgame.apps.expenses.expenses.Types;
import com.psixgame.apps.expenses.google.GoogleAccount;
import com.psixgame.apps.expenses.google.GooglePlayServices;
import com.psixgame.apps.expenses.utils.DateTimeProvider;
import com.psixgame.apps.expenses.utils.DefaultErrorHandler;
import com.psixgame.apps.expenses.utils.network.NetworkUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.joda.time.LocalDate;

import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;

import javax.inject.Inject;

public final class MainActivity extends BaseActivity {

    
    @Inject
    GooglePlayServices googlePlayServices;
    @Inject
    GoogleAccount googleAccount;
    @Inject
    NetworkUtils networkUtils;
    @Inject
    ExpensesRepository repository;
    @Inject
    DefaultErrorHandler globalErrorHandler;
    @Inject
    DateTimeProvider dateTimeProvider;

    private ActivityMainBinding binding;

    @Override
    protected final void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ObjectGraph.getInstance(getApplication()).getAppComponent().inject(this);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initUi();
    }

    @Override
    protected final void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        repository.addSyncingListener(syncingListener);
        repository.addPendingCountListener(countListener);
    }

    @Override
    protected final void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
        repository.removeSyncingListener(syncingListener);
        repository.removePendingCountListener(countListener);
    }

    private void initUi() {
        binding.tvDate.setOnClickListener(view -> onClickDate());
        binding.btnAdd.setOnClickListener(view -> onClickAdd());

        setSyncing(repository.isSyncing());
        setCount(0);

        //todo: set current date
        setAdapterToSpinner(binding.spType, getTypes());
        setAdapterToSpinner(binding.spSubtype, getSubtypes(binding.spType.getSelectedItem().toString()));

        binding.spType.setOnItemSelectedListener(typeSelectedListener);

        final LocalDate date = new LocalDate();
        setDateText(date.getDayOfMonth(), date.getMonthOfYear(), date.getYear());
    }

    private final Consumer<Integer> countListener = this::setCount;

    private final Consumer<Boolean> syncingListener = this::setSyncing;

    private void setSyncing(final boolean syncing) {
        binding.tvSyncing.setText(syncing ? "+" : "");
    }

    private void setCount(final int count) {
        binding.tvCount.setText(count > 0 ? count + "" : "");
    }

    private void setAdapterToSpinner(final Spinner spinner, final List<String> data) {
        final ArrayAdapter<String> typeAdapter = new ArrayAdapter<>(getApplication(),
                android.R.layout.simple_spinner_dropdown_item,
                data);
        spinner.setAdapter(typeAdapter);
    }

    private void onClickDate() {
        new DatePickerDialogFragment().show(getSupportFragmentManager(), "datePicker");
    }

    @Subscribe
    public final void onDateSetEvent(final DateSetEvent event) {
        setDateText(event.getDay(), event.getMonth() + 1, event.getYear());
    }

    private void setDateText(final int day, final int month, final int year) {
        binding.tvDate.setText(String.format(Locale.US, "%d/%d/%d", month, day, year));
    }

    private final AdapterView.OnItemSelectedListener typeSelectedListener = new AdapterView.OnItemSelectedListener() {

        @Override
        public final void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
            setAdapterToSpinner(binding.spSubtype, getSubtypes(parent.getItemAtPosition(position).toString()));
        }

        @Override
        public final void onNothingSelected(final AdapterView<?> parent) {
        }
    };

    private void onClickAdd() {
        try {
            Float.parseFloat(binding.etSpent.getText().toString());
            if (binding.etSpent.getText().toString().contains(",") ||
                    TextUtils.equals(spinnerNoSelectionText, binding.spType.getSelectedItem().toString())) {
                globalErrorHandler.unrecoverableError("Fill data");
                return;
            }
        } catch (final NumberFormatException e) {
            globalErrorHandler.unrecoverableError("Fill data");
            return;
        }

        checkPermissionsAndProceed();
    }

    private ExpenseData prepareData() {
        return new ExpenseData.Builder()
                .setCreatedAt(dateTimeProvider.getCurrentMillis())
                .setDate(binding.tvDate.getText().toString())
                .setSpent(Float.parseFloat(binding.etSpent.getText().toString()))
                .setType(binding.spType.getSelectedItem().toString())
                .setSubtype(binding.spSubtype.getSelectedItem().toString())
                .setComment(binding.etComment.getText().toString())
                .build();
    }

    private void clearUi() {
        binding.etSpent.setText("");
        binding.etComment.setText("");
    }

    private void checkPermissionsAndProceed() {
        googlePlayServices.check(this,
                () -> googleAccount.check(this, () -> {
                    repository.add(prepareData());
                    clearUi();
                }, globalErrorHandler::unrecoverableError),
                globalErrorHandler::unrecoverableError);
    }

    @Override
    protected final void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        googlePlayServices.onActivityResult(requestCode, resultCode, data);
        googleAccount.onActivityResult(requestCode, resultCode, data);
    }

    private final String spinnerNoSelectionText = "";

    private List<String> getTypes() {
        final List<String> res = Types.getTypes();
        res.add(0, spinnerNoSelectionText);
        return res;
    }

    private List<String> getSubtypes(final String type) {
        final List<String> res = Types.getSubtypes(type);
        res.add(0, spinnerNoSelectionText);
        return res;
    }

}
