package com.psixgame.apps.expenses.utils;

import android.app.Activity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public final class CurrentActivityProvider {

    private final List<Activity> activities = new ArrayList<>();

    
    @Inject
    public CurrentActivityProvider() {
    }

    
    public boolean hasActivity() {
        return activities.size() > 0;
    }

    
    public final Activity getActivity() {
        return activities.get(activities.size() - 1);
    }

    
    public final void setActivity(final Activity activity) {
        activities.add(activity);
    }

    
    public final void removeActivity(final Activity activity) {
        activities.remove(activity);
    }
}
