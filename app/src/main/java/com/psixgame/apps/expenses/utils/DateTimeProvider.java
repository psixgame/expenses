package com.psixgame.apps.expenses.utils;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public final class DateTimeProvider {

    
    @Inject
    DateTimeProvider() {
    }

    
    public final long getCurrentMillis() {
        return System.currentTimeMillis();
    }

}
