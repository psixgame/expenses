package com.psixgame.apps.expenses.utils;

import android.app.AlertDialog;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public final class DefaultErrorHandler {

    private final CurrentActivityProvider activityProvider;

    
    @Inject
    public DefaultErrorHandler(final CurrentActivityProvider activityProvider) {
        this.activityProvider = activityProvider;
    }

    
    public final void unrecoverableError(final String msg) {
        new AlertDialog.Builder(activityProvider.getActivity())
                .setMessage(msg)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    dialog.dismiss();
                })
                .create()
                .show();
    }

}
