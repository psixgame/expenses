package com.psixgame.apps.expenses.utils;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public final class KeyValueStorage {

    private final SharedPreferences preferences;

    
    @Inject
    KeyValueStorage(final Context context) {
        preferences = context.getSharedPreferences("shared_preferences", Context.MODE_PRIVATE);
    }

    
    public final void save(final String key, final String value) {
        preferences.edit().putString(key, value).commit();
    }

    
    public final String read(final String key) {
        return preferences.getString(key, null);
    }

    
    public final String read(final String key, final String defaultValue) {
        return preferences.getString(key, defaultValue);
    }

    
    public final boolean has(final String key) {
        return preferences.contains(key);
    }

    
    public final void remove(final String key) {
        preferences.edit().remove(key).commit();
    }

    
    public final void clear() {
        preferences.edit().clear();
    }

}
