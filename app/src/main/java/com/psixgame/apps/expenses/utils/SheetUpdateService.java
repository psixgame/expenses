package com.psixgame.apps.expenses.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.psixgame.apps.expenses.presentation.MainActivity;

public final class SheetUpdateService extends Service {

    @Nullable
    @Override
    public IBinder onBind(final Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(final Intent intent, final int flags, final int startId) {
        final Intent notificationIntent = new Intent(this, MainActivity.class);
        final PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, getPendingIntentFlag());
        final NotificationCompat.Builder builder = new NotificationCompat.Builder(this, createChannel())
                .setContentTitle("Expenses")
                .setContentText("Uploading expense")
                .setContentIntent(pendingIntent);
        final Notification notification = builder.build();
        startForeground(1233, notification);
        return START_STICKY;
    }

    private int getPendingIntentFlag() {
        if (VersionUtils.isSdk23OrMore()) {
            return PendingIntent.FLAG_IMMUTABLE;
        }
        return 0;
    }

    private static final String CHANNEL_ID = "expenses_channel";

    private String createChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            final NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_ID, NotificationManager.IMPORTANCE_DEFAULT);
            channel.setShowBadge(false);
            channel.setSound(null, null);
            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);
            return CHANNEL_ID;
        } else {
            return "";
        }
    }

}
