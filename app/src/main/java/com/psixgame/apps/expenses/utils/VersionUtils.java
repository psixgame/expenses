package com.psixgame.apps.expenses.utils;

import android.os.Build;

public final class VersionUtils {

    private VersionUtils() {}

    public static boolean isSdk23OrMore() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }
}
