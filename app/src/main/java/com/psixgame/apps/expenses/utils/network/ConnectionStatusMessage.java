package com.psixgame.apps.expenses.utils.network;

public final class ConnectionStatusMessage {

    private final boolean enabled;

    
    ConnectionStatusMessage(final boolean enabled) {
        this.enabled = enabled;
    }

    public final boolean isEnabled() {
        return enabled;
    }

    @Override
    public final String toString() {
        return "ConnectionStatusMessage{" +
                "enabled=" + enabled +
                '}';
    }
}
