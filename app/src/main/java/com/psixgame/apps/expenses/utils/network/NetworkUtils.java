package com.psixgame.apps.expenses.utils.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public final class NetworkUtils {

    private final Context context;

    
    @Inject
    NetworkUtils(final Context context) {
        this.context = context;
        registerNetworkReceiver();
    }

    @Deprecated
    
    public final boolean isDeviceOnline(final Context context) {
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    
    public final boolean isDeviceOnline() {
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    
    private void registerNetworkReceiver() {
        context.registerReceiver(networkReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    private final BroadcastReceiver networkReceiver = new BroadcastReceiver() {
        
        @Override
        public final void onReceive(final Context context, final Intent intent) {
            final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            final boolean connected = cm != null && cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();

            EventBus.getDefault().post(new ConnectionStatusMessage(connected));
        }

    };

}