Alias name: androiddebugkey
Creation date: Mar 16, 2017
Entry type: PrivateKeyEntry
Certificate chain length: 1
Certificate[1]:
Owner: C=US, O=Android, CN=Android Debug
Issuer: C=US, O=Android, CN=Android Debug
Serial number: 1
Valid from: Thu Mar 16 12:03:58 EET 2017 until: Sat Mar 09 12:03:58 EET 2047
Certificate fingerprints:
         MD5:  54:FE:7B:ED:05:2A:B8:D7:02:C1:92:35:A1:C6:40:D9
         SHA1: E9:BD:74:6B:9B:6E:6C:99:7F:B4:DF:0D:BD:CF:04:2C:EB:FA:E0:D2
         SHA256: C1:CA:8A:AD:13:ED:E6:4B:A2:9A:6D:95:E4:B1:20:2F:CC:0C:46:EB:74:18:C4:B6:C4:A7:31:5B:41:A2:9B:33
         Signature algorithm name: SHA1withRSA
         Version: 1